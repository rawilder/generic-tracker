class Item < ActiveRecord::Base
    def self.to_csv
        attributes = %w{name created_at status user} #customize columns here
    
        CSV.generate(headers: true) do |csv|
              csv << attributes
              all.each do |item|
                    csv << attributes.map{ |attr| item.send(attr) }
              end
        end
    end
    
    def self.search(search)
        if search
            where('name LIKE ?', "%#{search}%")
        else
            all
        end
    end
end
