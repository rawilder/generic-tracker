class HomeController < ApplicationController
    http_basic_authenticate_with name: Rails.application.secrets.admin_username, password: Rails.application.secrets.admin_password, except: [:show]
    
    def show
        # shows leaderboard
        # items = Item.where("created_at >= ?", 1.week.ago.utc)
        cutoff_record = CutoffDate.order("created_at").last
        if cutoff_record
            @cutoff = cutoff_record.date
        end
        if not @cutoff
            today = Date.today.in_time_zone("Central Time (US & Canada)")
            if not today.friday? or today <= today.noon
                items = Item.where("created_at >= ?", today.beginning_of_week(:friday).noon.in_time_zone("UTC"))
            else
                items = Item.where("created_at >= ?", today.yesterday.beginning_of_week(:friday).noon.in_time_zone("UTC"))
            end
        else
            items = Item.where("created_at >= ?", @cutoff.in_time_zone("UTC"))
        end
        users = ["Adam W.", "Andy K.", "Anthony D.", "Brian B.", "Damien O.", "Daniel W.", "Dennis W.", "Heamen J.", "Jacob M.", "Jamie O.", "Justin M.", "Lucas Q.", "Mark B.", "Michael P.", "Tommie N.", "Matt K."]
        users = users.sort
        @count = {}
        users.each do |user|
            @count[user] = 0
        end
        @total = 0
        items.each do |item|
            users.each do |user2|
                if item.user == user2
                    @count[user2] += 1
                    @total += 1
                end
            end
        end
        # people.sort_by { |name, age| age }
        @count = @count.sort_by { |user, amount| -amount }
    end
    
    def matt
        @cutoff_date = CutoffDate.order("created_at").last
        if @cutoff_date
            @cutoff_date[:date] = @cutoff_date[:date].in_time_zone("Central Time (US & Canada)")
        end
        @cutoff_date ||= CutoffDate.new
    end
    
    def post_matt
        cutoff = CutoffDate.new(cutoff_date_params)
        Time.zone = "Central Time (US & Canada)"
        date = Time.zone.parse(cutoff_date_params[:date])
        Time.zone = "UTC"
        cutoff.date = date
        if cutoff.save
            flash[:success] = "Cutoff (" + date.to_s + ") saved successfully."
        else
            flash[:error] = "Cutoff date not saved, check with Adam."
        end
        redirect_to root_path
    end

    private
    def cutoff_date_params
        params.require(:cutoff_date).permit(:date)
    end
end
