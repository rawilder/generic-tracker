class ItemController < ApplicationController
    http_basic_authenticate_with name: Rails.application.secrets.admin_username, password: Rails.application.secrets.admin_password, except: [:index, :show, :new, :create]
    
    def index
        @items = Item.search(params["cn"]).paginate(:page => params[:page], :per_page => 20).order('created_at DESC')
        @cert_count = Item.count
        @page = params[:page]
        respond_to do |format|
            format.html
            format.csv { send_data Item.all.to_csv, filename: "certs-#{Date.today}.csv" }
        end
    end
    
    def show
        @item = Item.find(params[:id])
    end
    
    def new
        @user= ""
        @certs = ""
        @status = ""
    end
    
    def create
        new_item_params = item_params
        successful = true
        if item_params["name"].empty? or item_params["user"].empty? or item_params["status"].empty?
            @certs = item_params["name"]
            @user = item_params["user"]
            @status = item_params["status"]
            flash[:danger] = "Please fill out all fields."
            render 'new'
        else
            item_params["name"].split(/[\r\n]+ /x).each do |new_item|
                new_item_params["name"] = new_item
                item = Item.new(new_item_params)
                successful = successful and item.save
            end
            if successful
                flash[:success] = "Cert(s) saved successfully."
            else
                flash[:error] = "One or more certs did not save successfuly, check with Adam."
            end
            redirect_to root_path
        end
    end
    
    def destroy
        Item.find(params[:id]).destroy
        flash[:success] = "Cert deleted"
        redirect_to root_path
    end
    
    private
    def item_params
        params.require(:item).permit(:user, :name, :status)
    end
  
end
