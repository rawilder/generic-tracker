class CreateCutoffDates < ActiveRecord::Migration
  def change
    create_table :cutoff_dates do |t|
      t.datetime :date
      
      t.timestamps null: false
    end
  end
end
